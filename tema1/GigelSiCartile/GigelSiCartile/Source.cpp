#include <iostream>
#include <fstream>
#include <list>
#include <string>

using namespace std;

int main()
{
	ifstream f("date.txt");

	int nr;
	char op;
	bool isUp = true;

	f >> nr;
	f.ignore();

	list<string> myList;
	for (int i = 1; i <= nr; ++i)
	{
		f >> op;
		f.ignore();

		if (op == 'A')
		{
			string name;
			getline(f, name);

			if (isUp)
				myList.push_back(name);
			else
				myList.push_front(name);
		}
		else
		{
			isUp = isUp ^ true;
		}
	}

	if (isUp)
	{
		for (auto& it : myList)
		{
			cout << it << "\n";
		}
	}
	else
	{	
		for (list<string>::reverse_iterator rit = myList.rbegin(); rit != myList.rend(); ++rit)
		{
			cout << *rit << "\n";
		}
	}
	system("pause");
	return 0;
}