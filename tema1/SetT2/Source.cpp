#include <set>
#include <list>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

ifstream f("date.txt");

struct Movie
{
	int year;
	string name;

	bool operator< (const Movie& other) const {
		if (this->name == other.name) {
			return this->year < other.year;
		}
		
		return this->name < other.name;
	}
};

int main()
{	
	int n;
	f >> n;

	list<int> sizeMovieList;
	for (int i = 1; i <= n; ++i)
	{	
		string nr;
		
		if (i != n) getline(f, nr, ',');
		else getline(f, nr);

		sizeMovieList.push_back(atoi(nr.c_str()));
	}

	set<Movie> ans;
	set<Movie> s;

	list<int>::iterator it = sizeMovieList.begin();

	string movieName, releaseYear;
	Movie movie;
	
	for (int i = 1; i <= *it; ++i)
	{
		getline(f, movieName, '#');
		getline(f, releaseYear);

		movie.name = movieName;
		movie.year = atoi(releaseYear.c_str());

		ans.insert(movie);
	}

	++it;
	for (; it != sizeMovieList.end(); ++it)
	{
		s.clear();
		for (int i = 1; i <= *it; ++i)
		{	
			getline(f, movieName, '#');
			getline(f, releaseYear);

			movie.name = movieName;
			movie.year = atoi(releaseYear.c_str());

			s.insert(movie);
		}

		set<Movie> result;
		set_intersection(ans.begin(), ans.end(), s.begin(), s.end(), inserter(result, result.begin()));

		ans = result;
	}

	set<Movie>::iterator it1;
	for (it1 = ans.begin(); it1 != ans.end(); ++it1)
	{
		cout << it1->name << ", " << it1->year << "\n";
	}

	system("pause");
	return 0;
}
