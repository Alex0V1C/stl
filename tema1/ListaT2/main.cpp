#include <iostream>
#include <fstream>
#include <algorithm>
#include <list>
using namespace std;

/*Definiti o structura cu doi membri intregi (x si y) in care sa retineti informatiile despre pozitia unui punct in plan.
Folositi o metoda care citeste dintr-un fisier n astfel de puncte: pe prima linie din fisier avem numarul de puncte,
apoi incepand cu linia 2 o sa gasim cate un punct pe linie dat ca X, Y.
 Afisati apoi continutul listei (X1,Y1) (X2, Y2).. (Xn,Yn) in ordinea crescatoare a distantei punctelor fata de
originea sistemului de coordonate*/

struct point{
    int x, y;
   // int d;
};

list<point> makeList()
{
    list<point> myList;
    ifstream f("date.in");

    int n, a, b;
    struct point p;
    f >> n;

    for (int i = 1; i <= n; ++i)
    {
        f >> p.x >> p.y;
        //p.d = sqrt(p.x * p.x + p.y * p.y);

        myList.push_back(p);
    }
    return myList;
}

struct
{
    bool operator()(point a, point b)
    {
        return (a.x * a.x + a.y * a.y < b.x * b.x + b.y * b.y);
    }
}comp;

void displayList (list<point> myList)
{
    list<point>::iterator it;
    cout << "Size: " << myList.size() << endl;
    cout << "Content: " << endl;

    for (it = myList.begin(); it != myList.end(); ++it)
    {
        cout << it->x<< " " << it->y << endl;
    }
    cout << endl;
}

int main()
{
    list<point> L;
    L = makeList();

    L.sort(comp);

    displayList(L);

    return 0;
}
