#include <iostream>
#include <vector>
#include <conio.h>
#include <fstream>

using namespace std;

vector<int> createVectorUsingFiles()
{
    ifstream f("date.in");
    ofstream g("date.out");

    vector<int> v;

    int n, val;
    f >> n;

    for (int i = 1; i <= n; ++i)
    {
        f >> val;
        v.push_back(val);
    }

    return v;
}
/*
Initialize and return a vector of n elements with
values from 0 to n-1
*/
vector<int> createAndInitializeVector(int n)
{
    // Constructor with default size
    vector<int> intVector;
    intVector.reserve(n);

    // Add n elements (will also increase the size)
    for (int i = 0; i < n ; i++)
    {
        /*
        O(1) Constant (amortized time, reallocation may happen).
        If a reallocation happens, the reallocation is itself up to linear in the entire size.
        */
        intVector.push_back(i);
    }

    return intVector;
}

/*
Output content without iterator
*/
void sizeAndContentWithoutIterator(vector<int> v)
{
    cout << "Vector size: " << v.size() << endl;

    cout << "Content:" << endl;
    for (int i = 0; i < v.size(); i++)
    {
        // Access element via index v[i]
        cout << v[i] << ", ";
    }

    cout << endl << endl;
}

/*
Output content with reverse iterators
*/
void sizeAndReverseContent(vector<int> v)
{
    cout << "Vector size: " << v.size() << endl;

    // Traverse the vector with a reverse iterator from back to top
    vector<int>::const_reverse_iterator it;
    for (it = v.rbegin(); it != v.rend(); it++)
    {
        cout << *it << ", "; // Iterator value accessed with *
    }

    cout << endl << endl;
}

/*
Output vector with iterators
*/
void sizeAndContent(vector<int> v)
{
    cout << "Vector size: " << v.size() << endl;

    // Traverse the vector with iterator
    vector<int>::iterator it;
    for (it = v.begin(); it != v.end(); it++)
    {
        cout << *it <<", ";
    }

    cout << endl << endl;
}

void vectorOps()
{
    // Default empty constructor
    vector<float> emptyVector;

    // Constructor with default value
    int n = 5;
    int val = 3;
    vector<int> filledVector(n, val);

    // Copy constructor
    vector<int> filledCopyVector(filledVector);

    // Assign a vector
    vector<int> createdVector = createAndInitializeVector(10);

    // Access elements - Constant complexity - O(1)
    cout << "Element with index 2: " << createdVector[2] << endl;
    createdVector[0] = 99;

    cout << "First element: " << createdVector.front() << endl;
    cout << "Last element: " << createdVector.back() << endl;

    // Iterate vector content
    cout << "Show vector in a normal order" << endl;
    sizeAndContentWithoutIterator(createdVector);

    cout << "Show vector in reverse order" << endl;
    sizeAndReverseContent(createdVector);

    // Resize vector.
    int newN = 16;
    int newVal = 100;
    /*
    Linear complexity O(n)
    Linear on the number of elements inserted/erased (constructions/destructions).
    If a reallocation happens, the reallocation is itself up to linear in the entire vector size.
    */
    createdVector.resize(newN, newVal);
    cout << "New vector" << endl;
    sizeAndContent(createdVector);

    // Expand vector
    vector<int>::iterator it;
    it = createdVector.begin();
    int index = 2;
    int howMany = 3;
    int expandval = 88;
    createdVector.insert(it + index, howMany, expandval); // Complexity O(n)
    cout << "Vector after insert" << endl;
    sizeAndContent(createdVector);

    // Delete elements
    index = 3;
    createdVector.erase(createdVector.begin(), createdVector.begin() + index); // Complexity O(n)
    sizeAndContent(createdVector);
}

int main()
{
    vector<int> v;
    v = createVectorUsingFiles();
    sizeAndContent(v);
    return 0;
}
