#include <iostream>
#include <list>

using namespace std;


bool isPalindrom (int x)
{
    if (x >= 0 && x <= 9)
        return true;

    int y = x;
    int nr = 0;
    int p = 1;

    while (y != 0)
    {
        nr = nr * p + y % 10;
        p *= 10;
        y /= 10;
    }

    return nr == x;
}

list<int> createAndInitializeList(int nrElems)
{
    list<int> l;
    for (int i = 0; i < nrElems; i++)
    {
        if (i < nrElems / 2)
            l.push_back(i); // O(1)
        else
            l.push_front(i); // O(1)
    }

    return l;
}

void listSizeAndContent(list<int> lst)
{
    if (lst.empty())
    {
        cout << "The list is empty" << endl;
        return;
    }

    cout << "List size: " << lst.size() << endl;

    list<int>::const_iterator it;
    it = lst.begin();

    while (it != lst.end())
    {
        cout << *it <<", ";
        it++;
    }

    cout <<" \n" ;
}

int main()
{
    list<int> l;

    l = createAndInitializeList(16);
    listSizeAndContent(l);

    l.remove_if(isPalindrom);
    listSizeAndContent(l);
    return 0;
}
