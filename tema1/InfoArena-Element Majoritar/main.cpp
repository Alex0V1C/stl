#include <map>
#include <string>
#include <fstream>

using namespace std;

int main()
{
	ifstream f("elmaj.in");
	ofstream g("elmaj.out");

	int n, x, i;
	f >> n;
	map<int, int> m;

	for (i = 1; i <= n; ++i)
	{
		f >> x;
		m[x]++;
	}

	map<int, int>::iterator p;
	map<int, int>::iterator it;

	int max = -1;

	for (it = m.begin(); it != m.end(); ++it)
	{
		if (it->second > max)
		{
			max = it->second;
			p = it;
		}
	}

	if (max < n / 2 + 1)
		g << "-1";
	else
		g << p->first << " " << p->second;

	//system("pause");
	return 0;
}
