#include <set>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <fstream>

using namespace std;

// Define a structure for a rectangle: x1, y1, x2, y2
// Define a constructor for the rectangle
struct rect
{
    int x1, y1, x2, y2;

    rect (int _x1, int _y1, int _x2, int _y2)
    {
        this->x1 = _x1;
        this->y1 = _y1;
        this->x2 = _x2;
        this->y2 = _y2;
    }
    // TODO create rectangle
};

int area(const rect& r)
{
    // TODO calculate area L x l (L = x2-x1; l = y2-y2)
    int L = abs(r.x2 - r.x1);
    int l = abs(r.y2 - r.y1);

    return l * L;
}

// Define operators: ==, <, based on the rectangle area
bool operator==(const rect& r1, const rect& r2)
{
    return area(r1) == area(r2);
}

bool operator<(const rect& r1, const rect& r2)
{
    return area(r1) < area(r2);
}

ostream& operator<<(ostream& os, const rect r)
{
    //os << "(" << r.x1 << "," << r.y1 << ":" << r.x2 << "," << r.y2 << ")";
    return os;
}

// Implement a method that  creates and initialize a set of 4 rectangles
set<rect> initSet(rect r1, rect r2, rect r3, rect r4)
{
    // TODO create and insert into set
    set<rect> s;
    //s.insert(r1);
    s.insert(r1);
    s.insert(r2);
    s.insert(r3);
    s.insert(r4);

    return s;
}

// Implement a method that output the content of the set using copy
void outputSet(set<rect> s)
{
    cout << "Set size: " << s.size()<< endl;

    copy(s.begin(), s.end(), ostream_iterator<rect>(cout, "; "));

    cout << endl;
}

int main()
{

    set<rect> s1, s2, s3;

    rect r1(0,0,10,10),
        r2(0,0,20,20),
        r3(0,0,30,30),
        r4(0,0,40,40),
        r5(10,10,20,20),
        r6(10,10,30,30),
        r7(0,0,15,15),
        r8(0,0,25,25);

    // Make the intersection of s1 and s2 and output the result in set s3
    set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), inserter(s3, s3.begin()));

    // Erase content of s3
    s3.clear();
    // or s3.erase(s3.begin(), s3.end());

    // Make the union of s1 and s2
    set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), inserter(s3, s3.begin()));

    // Test inclusions of s1 in s3
    cout << includes(s3.begin(), s3.end(), s1.begin(), s1.end());

}
