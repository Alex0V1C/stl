#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;

template <class Value>
class Network
{
	vector<Value> networkVector;
	size_t rotationPivot;

public:
	Network(const Value* arr, const int & offset)
	{
		const size_t sizeArr = sizeof(arr) / sizeof(arr[0]);

		for (size_t it = 0; it < sizeArr; ++it)
			networkVector.push_back(arr[it]);

		rotateWithOffset(offset);
		CalculateRotationPivot(offset);
	}

	void rotateWithOffset(size_t offset)
	{
		offset %= networkVector.size();
		rotate(networkVector.begin(), next(networkVector.begin(), offset), networkVector.end());
	}

	void CalculateRotationPivot(const size_t & offset)
	{
		(offset % networkVector.size() == 0) ? rotationPivot = 0 : rotationPivot = networkVector.size() - offset % networkVector.size();
	}

	void DisplayNetwork() const
	{
		for (const auto& val : networkVector)
			cout << val << " ";

		cout << "\n";
	}

	void DisplayRotationPivot()
	{
		cout << rotationPivot << "\n";
	}

	size_t Count() const
	{
		return networkVector.size();
	}

	const Value & GetData(size_t index) const
	{
		return networkVector[index];
	}

	typename vector<Value>::const_iterator BeginNodes() const
	{
		return networkVector.cbegin();
	}

	typename vector<Value>::const_iterator EndNodes() const
	{
		return networkVector.cend();
	}

	size_t GetNode(const Value & data) const
	{
		if (networkVector[rotationPivot] == data)
			return (size_t)rotationPivot;

		auto lw1 = lower_bound(networkVector.begin(), networkVector.begin() + rotationPivot, data);
		if (lw1 != networkVector.end() && *lw1 == data)
			return distance(networkVector.begin(), lw1);

		auto lw2 = lower_bound(networkVector.begin() + rotationPivot, networkVector.end(), data);
		if (lw2 != networkVector.end() && *lw2 == data)
			return distance(networkVector.begin(), lw2);

		cout << "The node was not found, we return the position of the end of the vector\n";
		return networkVector.size();
	}

};


int main()
{
	char* str = "ABCDEFGH";
	const size_t offset = 20;
	Network<char> *p = new Network<char>(str, offset);

	p->DisplayNetwork();

	cout << "\n";
	for (auto it = p->BeginNodes(); it != p->EndNodes(); ++it)
		cout << *it << "\n";
	cout << "\n";

	p->DisplayRotationPivot();

	cout << p->GetNode('C') << "\n";

	cout << p->Count() << "\n";
	cout << p->GetData(3) << "\n";

	system("pause");
	return 0;
}
