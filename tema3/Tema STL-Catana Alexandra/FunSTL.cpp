#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

const auto string1 = "algorithms";
const auto string2 = " ";
const auto string3 = "really love";
const auto string4= "!";

const auto function1(vector<string> & vector1)
{
	std::sort(vector1.begin(), vector1.end(), [](const auto &st1, const auto &st2)
	{
		return st1.size() < st2.size();
	});

	return std::accumulate(vector1.begin(), vector1.end(), string(), 
		[](const auto &s4, const auto &s5)
		{
			return (s4.empty() ? s5 : (s4 + string2)) + s5;
		});
}

int main()
{
	vector<string> ans = { string1, string3, string4 };
	cout << function1(ans) << "\n";

	//ans = !! algorithms really love
	system("pause");
	return 0;
}

